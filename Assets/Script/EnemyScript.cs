using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript : MonoBehaviour
{
    Rigidbody2D enemyRigidBody;
    public float moveSpeed = 2;
    int direction = 1;
    // Start is called before the first frame update
    void Start()
    {
        enemyRigidBody = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        enemyRigidBody.velocity = new Vector2(moveSpeed * direction, 0);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("Dead");
        direction *= -1;
        transform.localScale = new Vector2(Mathf.Sign(direction) * 5, 5);
    }
}
