using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    Rigidbody2D playerRB;
    public float moveSpeed;
    Animator playerAnimator;
    BoxCollider2D jumpCollider;
    CapsuleCollider2D playerCollider;
    public float climbSpeed = 2;
    public int coinsCollected;
    bool isAlive;
    public Transform bulletSpawnPoint;
    public GameObject bullet;
    public LineRenderer shootLine;

    // Start is called before the first frame update
    void Start()
    {
        isAlive = true;
        playerRB = GetComponent<Rigidbody2D>();
        playerAnimator = GetComponent<Animator>();
        jumpCollider = GetComponent<BoxCollider2D>();
        playerCollider = GetComponent<CapsuleCollider2D>();
        shootLine = GetComponent<LineRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (isAlive)
        {
            Move();
            Jump();
            Climb();
            FireRayCast();
            Die();
        }
    }

    private void Move()
    {
        float movement = Input.GetAxis("Horizontal");
        Vector2 playerVelocity = new Vector2(movement * moveSpeed, playerRB.velocity.y);
        playerRB.velocity = playerVelocity;

        bool playerHasHorizontalMovement = Mathf.Abs(playerRB.velocity.x) > 0;

        //if (playerHasHorizontalMovement)
        //{
        //    transform.localScale = new Vector2(Mathf.Sign(playerRB.velocity.x), 1);
        //}

        playerAnimator.SetBool("CanWalk", playerHasHorizontalMovement);

        if (Input.GetAxis("Horizontal") > 0)
        {
            transform.rotation = Quaternion.Euler(0, 0, 0);
        }
        else if (Input.GetAxis("Horizontal") < 0)
        {
            transform.rotation = Quaternion.Euler(0, 180, 0);
        }
    }

    private void Jump()
    {
        if (Input.GetKeyDown(KeyCode.Space) && jumpCollider.IsTouchingLayers(LayerMask.GetMask("Ground")))
        {
            playerAnimator.SetBool("CanJump", true);
            playerRB.velocity = new Vector2(0, 5);
        }
        else
        {
            playerAnimator.SetBool("CanJump", false);
        }
    }

    private void Climb()
    {
        if (playerCollider.IsTouchingLayers(LayerMask.GetMask("Climbing")))
        {
            if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W))
            {
                Vector2 climbVelocity = new Vector2(playerRB.velocity.x, climbSpeed);
                playerRB.velocity = climbVelocity;
                playerAnimator.SetBool("CanClimb", true);
            }
            else
            {
                playerRB.gravityScale = 0;
                playerRB.velocity = new Vector2(playerRB.velocity.x, 0);
                playerAnimator.SetBool("CanClimb", false);
            }
        }
        else
        {
            playerRB.gravityScale = 1;
        }
    }

    void Die()
    {
        if (playerCollider.IsTouchingLayers(LayerMask.GetMask("Enemy")))
        {
            isAlive = false;
            playerAnimator.SetTrigger("Dead");
            playerRB.velocity = new Vector2(10, 10);
        }
    }

    void Fire()
    {
        if(Input.GetMouseButtonDown(0))
        {
            Instantiate(bullet, bulletSpawnPoint.position, bulletSpawnPoint.rotation);
        }
    }

    void FireRayCast()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit2D hitInfo = Physics2D.Raycast(bulletSpawnPoint.position, bulletSpawnPoint.right);
            if (hitInfo)
            {
                Debug.Log(hitInfo.transform.gameObject);
                EnemyScript enemy = hitInfo.transform.GetComponent<EnemyScript>();
                if (enemy)
                {
                    Destroy(enemy.gameObject);
                }
                shootLine.SetPosition(0, bulletSpawnPoint.position);
                shootLine.SetPosition(1, bulletSpawnPoint.position + bulletSpawnPoint.right * 100);
            }
        }
    }
}
